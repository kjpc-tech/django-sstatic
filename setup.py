from setuptools import find_packages, setup

setup(
    name='django-sstatic',
    version='0.2',
    packages=find_packages(),
    author='Kyle (Originally Svyatoslav Bulbakha)',
    author_email='kyle@kjpc.tech',
    description='Simple static files versioning for django.',
    url='https://bitbucket.org/kjpc-tech/django-sstatic/',
    include_package_data=True
)

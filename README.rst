==============
django-sstatic
==============

Simple static files versioning for django.

This is a fork of https://bitbucket.org/ad3w/django-sstatic.

Quick start
-----------

1. Add "sstatic" to your INSTALLED_APPS.
2. Use the sstatic template tag

sstatic template tag
--------------------

    {% load sstatic %}

    <link rel="stylesheet" href="{% sstatic 'css/default.css' %}" />

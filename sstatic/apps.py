from django.apps import AppConfig


class SStaticConfig(AppConfig):
    name = 'sstatic'
    verbose_name = "Django SStatic"
